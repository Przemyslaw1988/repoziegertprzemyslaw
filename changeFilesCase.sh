#!/bin/bash
# first condition is checking if user typed only one parameter
if [ "$1" = "--help" ]; then
# display statement
        echo "HELP: Script changes leters in the name of files\n\vExample: plik on PLIK and on reverse"
# quit first condition
fi
	# Second condition to checked if first parameter is upper and next i upper 
	if [ "$#" -eq 2 ]; then
		# search the file in all directory 
  		for FILE in * ; do
		# third condition to secure the name of our script 
        if [ $0 != "$FILE" ] && [ $0 != "./$FILE" ]; then
		# this is change letter in the name of every file in current directory 
		mv "$FILE" "$(echo $FILE | tr [:$1:] [:$2:])";
   	fi
    		done
	# this the reverse of second condition 
 	if [ "$#" -eq 3 ]; then

		for FILE in * ; do
	if [ $0 != "$FILE" ] && [ $0 != "./$FILE" ]; then
		mv "$3" "$(echo $3 | tr [:$1:] [:$2:])";
	fi		 
	done
	fi

