#!/bin/bash
        # condition to display help
	if [ "$1" = "--help" ]; then
		# display statment 
		echo -e "HELP: Script changes one parameter to another.\n\vExample: dupa will be switched on zupa"
		# quit	condition 
		exit 0
	# end of checking conditions
	fi
	# First parameter 
	OLD="$1"
	# Second parameter
	NEW="$2"
	# Loop to find all directory and subdirectory
	for FILE in $(find . -type f); do
	# Condition if in directory or subdirectory exist file then...
  	if [ -f $FILE ] && [ -w $FILE ]; then
    	# Codition to blocked any changes inside script
	if [ $0 != "$FILE" ] && [ $0 != "./$FILE" ]; then

	# In this plik change the phrase
   	sed -i -- "s/$OLD/$NEW/g" "$FILE" 
  	# end condition
   	fi
	fi
	# end script
	done
	
