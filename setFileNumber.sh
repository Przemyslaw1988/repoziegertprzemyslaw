#!/bin/bash	
	# first condition is checking the value of variables is greater than 0
	if [ "$#" -gt 0 ] ; then
		# second cond. is checking the value of variables is equal to 1
    		if [ "$#" -eq 1 ] ; then
			# third condition is checking the first of variables is equal to --help
        		if [ "$1" = "--help" ] ; then
				# display standard data on input and the next to the output
				echo  -e "tt.sh <director>|--help create the directory and some files in it and try on\n\v Example: Student : 0_ Student" 
	# calling condition: otherwise $1 is a directory 
        elif [ -d $1 ] ; then
	# point the $1 or quit
           	cd "$1" || exit
		# assign the value integer
	    	declare -i COUNTER= 0
		# loop for a file 
	   	for FILE in * 
	   	do

		
			# function to change the name of file
			mv ${FILE} ${COUNTER}_${FILE}
			# increment
			$((++COUNTER))

		# end of loop
   		done
        	else
			# display statment 
            		echo -e "wrong argument, type --help to get help."
		# end of second condition
		fi
  			 else
		# display statment
        	echo -e "too many arguments, type --help to get help."
		# quit second condition
        	exit 1
		# end of third condition
   			 fi
    
	else
		# display statement
   		 echo -e "missed argument, type --help to get help."
	# quit first and third condition
   	exit 1
	# script end
	fi		
